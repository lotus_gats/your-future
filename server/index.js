/* config */
const config = require('./config');

/* app */
const express = require('express');
const app = express();

/* logger */
const logger = require('morgan');
app.use(logger('tiny'));

/* CORS */
const cors = require('cors');
app.use(cors());

/* body-parser */
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/* model */
const db = require('./models');

/* controller */
const router = express.Router();
const controller = require('./controller');
app.use('/api/v1', controller(router, db));

/* launch */
app.listen(config.port, () => {
    console.log(`Server is listening on ${config.port} port...`)
});