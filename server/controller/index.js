module.exports = (router, db) => {
    const Users = require('./users')(db);

    return router
        .get('/users', (req, res) => {
            Users.getAll(req, res)
        })
        .post('/user', (req, res) => {
            Users.create(req, res)
        })
        .patch('/user/:_id?', (req, res) => {
            Users.update(req, res)
        })
        .delete('/user/:_id?', (req, res) => {
            Users.remove(req, res)
        })
        .get('/users/sort/:field?', (req, res) => {
            Users.sort(req, res);
        })
        .get('/users/search/:field?/:value?', (req, res) => {
            Users.search(req, res);
        })
        .post('/users/filter', (req, res) => {
            Users.filter(req, res);
        });
};