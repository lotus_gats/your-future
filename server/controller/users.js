module.exports = (db) => ({
    getAll: async (req, res) => {
        try {
            const users = await db.users.find();
            res.status(200).send(users)
        } catch (e) {
            console.log(e);
            res.status(500).send({error: 'Server error'})
        }
    },
    create: async (req, res) => {
        try {
            const user = await db.users.create(req.body);
            res.status(201).send(user);
        } catch (e) {
            console.log(e);
            res.status(400).send({error: 'Validation error'})
        }
    },
    update: async (req, res) => {
        try {
            const user = await db.users.findOneAndUpdate({_id: req.param('_id')}, {$set: req.body}, {new: true});
            res.status(200).send(user);
        } catch (e) {
            console.log(e);
            res.status(404).send({error: 'User or his fields not found'});
        }
    },
    remove: async (req, res) => {
        try {
            const {_id} = req.params;
            await db.users.deleteOne({_id});
            res.status(200).send({success: 'User is removed'});
        } catch (e) {
            console.log(e);
            switch (e.name) {
                case 'CastError':
                    res.status(404).send({error: 'User not found'});
                    break;
                default:
                    res.status(500).send({error: 'Server error'});
            }
        }
    },
    sort: async (req, res) => {
        try {
            const field = req.param('field');
            const users = await db.users.find().sort({
                [field]: 1
            });
            res.status(200).send(users);
        } catch (e) {
            console.log(e);
            res.status(400).send({error: 'Field not found'})
        }
    },
    search: async (req, res) => {
        try {
            const field = req.param('field');
            const value = req.param('value');
            const users = parseInt(value) ?
                await db.users.find({
                    [field]: parseInt(value)
                })
                : await db.users.find({
                    [field]: {
                        $regex: value,
                        $options: 'i'
                    }
                });
            res.status(200).send(users);
        } catch (e) {
            console.log(e);
            res.status(400).send({error: 'Field not found'})
        }
    },
    filter: async (req, res) => {
        try {
            const {field, values} = req.body;
            const users = await db.users.find({
                $or: values.map(value => ({[field]: value}))
            });
            res.status(200).send(users);
        } catch (e) {
            console.log(e);
            res.status(400).send({error: 'Field not found'})
        }
    }
});