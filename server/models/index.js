const mongoose = require('mongoose');
const {username, password} = require('../config').db;
const url = `mongodb://${username}:${password}@ds223605.mlab.com:23605/your-future`;
const options = {
    useNewUrlParser: true
};

(async () => {
    try {
        await mongoose.connect(url, options);
        console.log('Mongo database is connected...');
    } catch (e) {
        console.error('Mongo database isn`t connected...');
        console.error(e);
    }
})();

module.exports = {
    users: require('./users')
};