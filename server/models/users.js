const mongoose = require('mongoose');

module.exports = mongoose.model('User', new mongoose.Schema({
    firstName: {
        type: String,
        required: true
    },
    patronymic: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    dateOfBirth: {
        type: String,
        required: true
    },
    profession: {
        type: String,
        enum: ['Junior', 'Middle', 'Senior'],
        required: true
    },
    skills: {
        type: Array,
        required: true
    },
    salary: {
        type: Number,
        required: true
    }
}, {
    versionKey: false
}));