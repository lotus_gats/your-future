import React from 'react';
import './index.scss';
import connect from "react-redux/es/connect/connect";
import {toggleModal} from "../../actions/modal";
import {searchUser, sortUsers, filterUsers} from "../../actions/users";
import checkboxes from './checkboxer';

class Filter extends React.Component{
    state = {
        filter: ['Junior', 'Middle', 'Senior']
    };

    componentWillUpdate(nextProps, nextState) {
        if (this.state.filter.length !== nextState.filter.length) {
            this.props.filterUsers(nextState.filter)
        }
    }

    openModal = () => {
        this.props.toggleModal({
            isOpen: true,
            formAction: '/user',
            formMethod: 'POST'
        });
    };

    search = (el) => {
        const lastName = el.value;
        this.props.searchUser(lastName);
    };

    sort = (field) => {
        this.props.sortUsers(field);
    };

    filter = (value) => {
        this.setState(prevState => ({
            filter: prevState.filter.some(item => item === value)
                ? prevState.filter.filter(item => item !== value)
                : [...prevState.filter, value]
        }));
    };

    filterIsActive = (value) => !this.state.filter.some(item => item === value);

    render() {
        return (
            <section className={'filter'}>
                <h2>Select required users...</h2>
                <input type={'text'} placeholder={'Last name...'} onChange={(e) => {this.search(e.target)}}/>
                <div className={'sorter'}>
                    <button onClick={() => {this.sort('salary')}}>Sort by salary</button>
                    <button onClick={() => {this.sort('lastName')}}>Sort by last name</button>
                </div>
                <div className={'professions'}>
                    <h2>Filter</h2>
                    {checkboxes([{
                        name: 'Junior',
                        isDisable: this.filterIsActive('Junior'),
                        onClick: this.filter
                    }, {
                        name: 'Middle',
                        isDisable: this.filterIsActive('Middle'),
                        onClick: this.filter
                    }, {
                        name: 'Senior',
                        isDisable: this.filterIsActive('Senior'),
                        onClick: this.filter
                    }])}
                </div>
                <button onClick={() => {this.openModal()}}>Add new</button>
            </section>
        )
    }
}

export default connect(
    state => ({}),
    dispatch => ({
        toggleModal: (data) => {
            dispatch(toggleModal(data))
        },
        searchUser: async (lastName) => {
            dispatch(await searchUser(lastName))
        },
        sortUsers: async (field) => {
            dispatch(await sortUsers(field))
        },
        filterUsers: async (values) => {
            dispatch(await filterUsers(values))
        }
    })
)(Filter);