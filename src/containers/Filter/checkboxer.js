import React from 'react';

export default (items) => (
    items.map((item, index) =>  <button key={index}
                                        className={item.isDisable ? 'disable' : ''}
                                        onClick={() => {item.onClick(item.name)}}
    >{item.name}</button>)

)