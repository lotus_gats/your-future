import React from 'react';
import ModalItem from './item';
import './index.scss';
import {addUser, editUser} from "../../actions/users";
import {toggleModal} from "../../actions/modal";
import connect from "react-redux/es/connect/connect";

class Modal extends React.Component {
    state = {
        fields: {
            firstName: '',
            patronymic: '',
            lastName: '',
            profession: '',
            salary: 0,
            skills: [],
            dateOfBirth: '',
        },
        isOpen: false,
        method: 'POST'
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (prevState.isOpen !== nextProps.modal.isOpen) {
            return {
                fields: nextProps.modal.fields,
                isOpen: nextProps.modal.isOpen,
                method: nextProps.modal.formMethod
            };
        }
        return prevState;
    }

    close = (e) => {
        if (e) {
            e.preventDefault();
        }
        this.props.toggleModal({
            isOpen: false
        });
    };

    onKeyPress = (el, field) => {
        const {value} = el;

        field === 'skills' ?
            this.setState(prevState => ({
                fields: Object.assign({}, prevState.fields, {[field]: value.split(',')})
            })) : this.setState(prevState => ({
                fields: Object.assign({}, prevState.fields, {[field]: value})
            }));
    };

    validate = async (object) => {

        for (let key in object) {

            if (object.hasOwnProperty(key)) {

                if (key !== 'salary' && !object[key].length) {
                    alert('Fields are empty');
                    return false;
                }
            }
        }
        return true;
    };

    submit = async (e) => {
        e.preventDefault();
        const {fields, method} = this.state;

        if (await this.validate(fields)) {
            method === 'POST' ?
                this.props.addUser(fields)
                : this.props.editUser(fields._id, fields);
            this.close();
        }
    };

    toFill = (data) => ({
        onKeyPress: this.onKeyPress,
        name: data.name,
        label: data.label,
        value: this.state.fields[data.name],
        placeholder: data.placeholder || '',
        type: data.type || 'text'
    });

    render() {
        const {modal} = this.props;
        const {isOpen, fields} = this.state;

        if (isOpen) {
            return (
                <div className={`modal--overlay ${isOpen ? 'show' : ''}`}>
                    <div className={'modal--window'}>
                        <h2>NEW USER</h2>
                        <button className={'modal--window__close'}
                           onClick={(e) => {
                               this.close(e)
                           }}
                        />
                        <form action={modal.formAction} method={modal.formMethod}>
                            {ModalItem(this.toFill({
                                name: 'firstName',
                                label: 'First name'
                            }))}
                            {ModalItem(this.toFill({
                                name: 'patronymic',
                                label: 'Patronymic'
                            }))}
                            {ModalItem(this.toFill({
                                name: 'lastName',
                                label: 'Last name'
                            }))}
                            {ModalItem(this.toFill({
                                name: 'profession',
                                label: 'Profession',
                                placeholder: 'Junior/Middle/Senior'
                            }))}
                            <div>
                                <div className={'line'}/>
                                <div className={'modal--window__item'}>
                                    <div className={'modal--window__item__half'}>
                                        <p>Skills</p>
                                    </div>
                                    <div className={'modal--window__item__half'}>
                                    <textarea name={'skills'}
                                              placeholder={'To next skill press ,'}
                                              onChange={(e) => {
                                                  this.onKeyPress(e.target, 'skills')
                                              }}
                                              value={fields.skills.length ? fields.skills.map(skill => `${skill}`) : ''}
                                    />
                                    </div>
                                </div>
                            </div>
                            {ModalItem(this.toFill({
                                name: 'salary',
                                label: 'Salary',
                                type: 'number'
                            }))}
                            {ModalItem(this.toFill({
                                name: 'dateOfBirth',
                                label: 'Date of birth',
                                placeholder: '27 March, 2000'
                            }))}
                            <div className={'modal--window__sbm--block'}>
                                <input type={'submit'}
                                       value={'Confirm'}
                                       onClick={async (e) => {await this.submit(e)}}
                                />
                            </div>
                        </form>
                    </div>
                </div>
            )
        }
        else {
            return  <div className={`modal--overlay`}/>
        }
    }
}

export default connect(
    state => ({
        users: state.users,
        modal: state.modal
    }),
    dispatch => ({
        addUser: async (user) => {
            dispatch(await addUser(user))
        },
        editUser: async (id, user) => {
            dispatch(await editUser(id, user))
        },
        toggleModal: async (isOpen, fields) => {
            dispatch(await toggleModal(isOpen, fields))
        }
    })
)(Modal);