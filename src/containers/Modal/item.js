import React from 'react';

export default (data) => (
    <div>
        <div className={'line'}/>
        <div className={'modal--window__item'}>
            <div className={'modal--window__item__half'}>
                <p>{data.label}</p>
            </div>
            <div className={'modal--window__item__half'}>
                <input type={data.type}
                       name={data.name}
                       placeholder={data.placeholder}
                       onChange={(e) => {data.onKeyPress(e.target, data.name)}}
                       value={data.value}
                />
            </div>
        </div>
    </div>
);
