import React from 'react';
import connect from "react-redux/es/connect/connect";
import {getUsers, editUser, removeUser} from "../../actions/users";
import Item from "./Item";
import './index.scss';
import {toggleModal} from "../../actions/modal";

class List extends React.Component{
    componentDidMount() {
        this.props.getUsers();
    }

    edit = (id, fields) => {
        this.props.editUser(id, fields);
    };

    remove = (id) => {
        this.props.removeUser(id);
    };

    openModal = (fields) => {
        this.props.toggleModal({
            isOpen: true,
            formAction: `/user/${fields._id}`,
            formMethod: 'PATCH',
            fields
        });
    };

    render() {
        const {users} = this.props;

        if (users.length) {
            return (
                <section className={'list'}>
                    <div className={'wrapper'}>
                        <h2>Users</h2>
                        <hr/>
                        <div className={'list__content'}>
                            {users.map(user => (
                                <Item key={user._id}
                                      {...user}
                                    onModalOpen={() => {this.openModal(user)}}
                                    onEdit={(fields) => {this.edit(user._id, fields)}}
                                    onRemove={() => {this.remove(user._id)}}
                                />))}
                        </div>
                    </div>
                </section>
            )
        }
        else {
            return (
                <p>Users not found</p>
            )
        }
    }
}

export default connect(
    state => ({
        users: state.users
    }),
    dispatch => ({
        getUsers: async () => {
            dispatch(await getUsers())
        },
        editUser: async (id, fields) => {
            dispatch(await editUser(id, fields))
        },
        removeUser: async (id) => {
            dispatch(await removeUser(id))
        },
        toggleModal: async (status, fields) => {
            dispatch(await toggleModal(status, fields))
        }
    })
)(List);