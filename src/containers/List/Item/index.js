import React from 'react';
import PropTypes from 'prop-types';
import avatar from '../../../../public/avatar.png';
import './index.scss';

class Item extends React.Component{
    static propTypes = {
        _id: PropTypes.string.isRequired,
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        patronymic: PropTypes.string.isRequired,
        profession: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string).isRequired,
        salary: PropTypes.number.isRequired,
        dateOfBirth: PropTypes.string.isRequired,
        onRemove: PropTypes.func.isRequired,
        onModalOpen: PropTypes.func.isRequired
    };

    render() {
        const {firstName, lastName, patronymic, profession, skills, salary, dateOfBirth} = this.props;
        return (
            <div className={'list__item'}>
                <div className={'list__item__img--block'}>
                    <img src={avatar} alt={'avatar'}/>
                </div>
                <div className={'list__item__desc--block'}>
                    <h3>{firstName} {patronymic} {lastName}</h3>
                    <h4>Profession: {profession}</h4>
                    <h5>Skills:</h5>
                    {skills.map((skill, index) => (
                        <p key={index}>{skill}</p>
                    ))}
                    <p>Salary: {salary}$</p>
                    <p>Date of birth: {dateOfBirth}</p>
                </div>
                <button className={'list__item__edit--btn'} onClick={() => {this.props.onModalOpen()}}>
                    <svg version="1.1" className="edit-svg" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 485.219 485.22">
                        <g>
                            <path d="M467.476,146.438l-21.445,21.455L317.35,39.23l21.445-21.457c23.689-23.692,62.104-23.692,85.795,0l42.886,42.897
	    	                    C491.133,84.349,491.133,122.748,467.476,146.438z M167.233,403.748c-5.922,5.922-5.922,15.513,0,21.436
		                        c5.925,5.955,15.521,5.955,21.443,0L424.59,189.335l-21.469-21.457L167.233,403.748z M60,296.54c-5.925,5.927-5.925,15.514,0,21.44
		                        c5.922,5.923,15.518,5.923,21.443,0L317.35,82.113L295.914,60.67L60,296.54z M338.767,103.54L102.881,339.421
		                        c-11.845,11.822-11.815,31.041,0,42.886c11.85,11.846,31.038,11.901,42.914-0.032l235.886-235.837L338.767,103.54z
		                        M145.734,446.572c-7.253-7.262-10.749-16.465-12.05-25.948c-3.083,0.476-6.188,0.919-9.36,0.919
		                        c-16.202,0-31.419-6.333-42.881-17.795c-11.462-11.491-17.77-26.687-17.77-42.887c0-2.954,0.443-5.833,0.859-8.703
		                        c-9.803-1.335-18.864-5.629-25.972-12.737c-0.682-0.677-0.917-1.596-1.538-2.338L0,485.216l147.748-36.986
		                        C147.097,447.637,146.36,447.193,145.734,446.572z"></path>
                        </g>
                    </svg>
                </button>
                <button className={'list__item__delete--btn'}
                   onClick={(_id) => {this.props.onRemove(_id)}}
                />
            </div>
        )
    }
}

export default Item