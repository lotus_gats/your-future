import React from 'react';
import Filter from "../Filter";
import './reset.scss';
import List from "../List";
import Modal from "../Modal";

class App extends React.Component{
    render() {
        return (
            <div>
                <Filter/>
                <List/>
                <Modal/>
            </div>
        )
    }
}

export default App