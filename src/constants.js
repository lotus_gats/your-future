export const actionTypes = {
    getUsers: 'GET_USERS',
    addUser: 'ADD_USER',
    editUser: 'EDIT_USER',
    removeUser: 'REMOVE_USER',
    searchUser: 'SEARCH_USER',
    sortUsers: 'SORT_USERS',
    filterUsers: 'FILTER_USERS',
    toggleModal: 'TOGGLE_MODAL'
};

export const API = {
    hostname: 'http://127.0.0.1:3001/api/v1'
};