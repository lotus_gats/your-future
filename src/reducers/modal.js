import {actionTypes} from '../constants';
import {API} from "../constants";

const defaultState = {
    isOpen: false,
    fields: {
        firstName: '',
        patronymic: '',
        lastName: '',
        profession: '',
        salary: 0,
        skills: [],
        dateOfBirth: '',
    },
    formAction: '',
    formMethod: ''
};

export const modal = (state = defaultState, action) => {
    switch (action.type) {
        case actionTypes.toggleModal:
            const fields = action.payload.fields || defaultState.fields;
            const {isOpen, formAction, formMethod} = action.payload;
            return Object.assign({}, state, {isOpen, fields, formAction: `${API.hostname}${formAction}`, formMethod});
        default:
            return state;
    }
};