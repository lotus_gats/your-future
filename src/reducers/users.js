import {actionTypes} from '../constants';

const defaultState = [];

export const users = (state = defaultState, action) => {
    switch (action.type) {
        case actionTypes.getUsers:
            return action.payload.users;
        case actionTypes.addUser:
            return [...state, action.payload.user];
        case actionTypes.editUser:
            return [...state.filter(user => user._id !== action.payload.id), action.payload.user];
        case actionTypes.searchUser:
            return action.payload.users;
        case actionTypes.sortUsers:
            return action.payload.users;
        case actionTypes.filterUsers:
            return action.payload.users;
        case actionTypes.removeUser:
            return state.filter(user => user._id !== action.payload.id);
        default:
            return state;
    }
};