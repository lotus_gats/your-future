import {actionTypes} from "../constants";
import {API} from "../constants";
import axios from 'axios';

export const getUsers = async () => {
    try {
        const res = await axios.get(`${API.hostname}/users`);
        return {
            type: actionTypes.getUsers,
            payload: {
                users: res.data
            }
        }
    } catch (e) {
        console.log(e.response);
    }
};

export const addUser = async (user) => {
    try {
        const res = await axios.post(`${API.hostname}/user`, user);
        return {
            type: actionTypes.addUser,
            payload: {
                user: res.data
            }
        }
    } catch (e) {
        console.log(e.response);
    }
};

export const editUser = async (id, fields) => {
    try {
        const res = await axios.patch(`${API.hostname}/user/${id}`, fields);
        return {
            type: actionTypes.editUser,
            payload: {
                id,
                user: res.data
            }
        }
    } catch (e) {
        console.log(e.response);
    }
};

export const removeUser = async (id) => {
    try {
        await axios.delete(`${API.hostname}/user/${id}`,);
        return {
            type: actionTypes.removeUser,
            payload: {id}
        }
    } catch (e) {
        console.log(e.response);
    }
};

export const searchUser = async (lastName) => {
    try {
        const res = lastName.length ?
            await axios.get(`${API.hostname}/users/search/lastName/${lastName}`)
            : await axios.get(`${API.hostname}/users`);
        return {
            type: actionTypes.searchUser,
            payload: {users: res.data}
        }
    } catch (e) {
        console.log(e.response);
    }
};

export const sortUsers = async (field) => {
    try {
        const res = await axios.get(`${API.hostname}/users/sort/${field}`);
        return {
            type: actionTypes.sortUsers,
            payload: {
                users: res.data
            }
        }
    } catch (e) {
        console.log(e.response);
    }
};

export const filterUsers = async (values) => {
    try {
        const res = await axios.post(`${API.hostname}/users/filter`, {field: 'profession', values});
        return {
            type: actionTypes.filterUsers,
            payload: {
                users: res.data
            }
        }
    } catch (e) {
        console.log(e.response);
    }
};