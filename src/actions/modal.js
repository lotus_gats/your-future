import {actionTypes} from "../constants";

export const toggleModal =  (data) => ( {
    type: actionTypes.toggleModal,
    payload: data
});
